var createError = require('http-errors');
var express = require('express');
var cors = require('cors');
var path = require('path');
var bodyParser = require('body-parser');
var mongoose=require('mongoose');
var indexRouter = require('./routes/index');
var app = express();
app.use(cors());
mongoose.connect('mongodb://localhost:27017/Marsupilami', { useNewUrlParser: true });
app.use(express.json());
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.resolve(__dirname)));
app.use('/', indexRouter);

app.use(function(req, res, next) {
  next(createError(404));
});
module.exports = app;
app.listen(process.env.PORT || 3000, process.env.IP || '0.0.0.0' );

