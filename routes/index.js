var express = require('express');
var router = express.Router();
var Marsupilami = require('../models/Marsupilami');
var Friend = require('../models/Friend');
var jwt=require('jsonwebtoken');

//---------------------------------------------------------
//To verify Token

function verifyToken(req,res,next){
    if(!req.headers.authorization){
      return  res.status(401).json({message: "Unauthorized request"});
    }
    var token =req.headers.authorization.split(' ')[1];
    if(token==='null'){
       return  res.status(401).json({message: "Unauthorized request"});

    }
    var payload=jwt.verify(token,'secretKey');
    if(!payload){
       return  res.status(401).json({message: "Unauthorized request"});
    }
    req.userID=payload.subject;
    next();
}


// Connection verify
router.post('/connection-verify', function (req, res, next) {
    Marsupilami.findOne({'marsupilami_email':req.body.email},function(err,marsupilami){
        if (err){
            return res.status(501).json(err);
        }
        if(!marsupilami) {
            return res.status(404).json({message:'Il faut créer un compte avant de connecter'});
        }
        else if(!marsupilami.validPassword(req.body.password)) {
            return res.status(404).json({message:'Le mot de passe  est incorrect'});
        }
        else{
            var payload={subject :marsupilami._id};
            var token = jwt.sign(payload,'secretKey');
            return res.status(200).json({token:token});
        }
    });
});
//--------------------------------------------------------

// Post Date new Masupilami
router.post('/post-data-marsupilami', function (req, res, next) {
    Marsupilami.findOne({'marsupilami_email':req.body.email},function(err,marsupilami){
        if (err){return res.status(404).json({err:err});  }
        if(marsupilami){
            return res.status(404).json({message:'Email déjà en utilisation'});
        }
        var newMarsupilami= new Marsupilami();
        newMarsupilami.marsupilami_Race= req.body.race;
        newMarsupilami.marsupilami_Famille= req.body.famille ;
        newMarsupilami.marsupilami_Nourriture= req.body.nourriture;
        newMarsupilami.marsupilami_Age= req.body.age ;
        newMarsupilami.marsupilami_email= req.body.email;
        newMarsupilami.marsupilami_password= newMarsupilami.encryptPassword(req.body.password);
        newMarsupilami.save(function (err, result) {
            if (err){
               return res.status(404).json({err:err});
            }
            var payload={subject :newMarsupilami._id};
            var token = jwt.sign(payload,'secretKey');
            return res.status(200).json({token:token});

        });
    });
});
//---------------------------------------------------------

//get ID new Added friend  by email
router.post('/FriendID',verifyToken, function (req, res, next) {
    console.log("email recieved : "+req.body.email);
        Marsupilami.findOne({'marsupilami_email': req.body.email}, function (err, Marsupilami) {
            if (err) {
                return res.status(501).json(err);
            }
            console.log("ID To Send : "+Marsupilami._id);
            res.status(201).json({idFriend: Marsupilami._id});
        });
});






// Post Save Changes

router.post('/saveChanges',verifyToken, function (req, res, next) {
    Marsupilami.updateOne({'_id':req.userID}, {
        marsupilami_Race: req.body.race,
        marsupilami_Famille: req.body.famille,
        marsupilami_Nourriture: req.body.nourriture,
        marsupilami_Age: req.body.age
    }, function (err, affected, resp) {

        Marsupilami.findOne({'_id': req.userID}, function (err, NewMarsupilami) {
            if (err) {
                return done(err);
            }
            res.status(201).json({marsupilami_Information: NewMarsupilami});
        });
    });
});
//---------------------------------------------------------

//marsupilami Change profil
router.get('/Modifier_Information',verifyToken, function (req, res, next) {
       Marsupilami.findOne({'_id': req.userID}, function (err, marsupilami) {
        if (err) {
            res.status(204).json({marsupilami_Information: marsupilami});
        }
        res.status(200).json({marsupilami_Information: marsupilami});
    });


});

//------------------------------------------------------

//Marsupilami get profil

router.get('/myProfil', verifyToken,function (req, res, next) {
    Marsupilami.find({}, function (err, docs) {
        var marsupilami_List = docs.filter(function (doc) {
            return doc._id != req.userID;
        });
        Marsupilami.findOne({'_id': req.userID}, function (err, marsupilami) {
            var Friend_email_List = [];
            marsupilami.marsupilami_friends.forEach(function (doc) {
                Friend_email_List.push(doc.friend_email);
            });
            Friend_email_List.forEach(function(email) {
                marsupilami_List.forEach(function(element) {
                    var index = marsupilami_List.findIndex(function (element) {
                        return email === element.marsupilami_email;
                    });
                    if(index !== -1){
                        marsupilami_List.splice(index, 1);
                    }
                });
            });
            res.status(201).json({
                marsupilami_Information: marsupilami,
                marsupilami_List: marsupilami_List
            });

        });
    });


});

//------------------------------------------------------
//Marsupilami add Friend

router.get('/addFriend/:id', verifyToken, function (req, res, next) {
    var friend_id = req.params.id;
    Marsupilami.findOne({'_id': req.userID}, function (err, marsupilami) {
        Marsupilami.findOne({'_id': friend_id}, function (err, marsupilamiFriend) {

            if (err) {
                res.status(300).json({message: "check ID"});
            } else {

                var index = marsupilami.marsupilami_friends.findIndex(function (element) {
                    return element.friend_email === marsupilamiFriend.marsupilami_email;
                });
                if (index !== -1) {
                    res.status(201).json({message: "Friend already in the list"});
                } else {
                    var friend = new Friend();
                    friend.friend_email = marsupilamiFriend.marsupilami_email;
                    friend.friend_RealID = marsupilamiFriend._id;
                    friend.friend_Race = marsupilamiFriend.marsupilami_Race;
                    friend.friend_Famille = marsupilamiFriend.marsupilami_Famille;
                    friend.friend_Nourriture = marsupilamiFriend.marsupilami_Nourriture;
                    friend.friend_Age = marsupilamiFriend.marsupilami_Age;
                    marsupilami.marsupilami_friends.push(friend);

                    Marsupilami.updateOne({'_id': req.userID}, {
                        marsupilami_friends: marsupilami.marsupilami_friends
                    }, function (err, affected, resp) {
                        res.status(201).json({message: "Friend added"});
                    });
                }
            }
        });
    });
});

//------------------------------------------------------

//Marsupilami delete Friend

router.get('/deleteFriend/:id', verifyToken, function (req, res, next) {
    var friend_id = req.params.id;
    Marsupilami.findOne({'_id': req.userID}, function (err, marsupilami) {

        Marsupilami.findOne({'_id': friend_id}, function (err, marsupilamiFriend) {

            if (err) {
                res.status(300).json({message: "check ID"});
            } else {

                if (marsupilami.marsupilami_friends.length > 0) {
                    var index = marsupilami.marsupilami_friends.findIndex(function (element) {
                        return element.friend_email === marsupilamiFriend.marsupilami_email;
                    });
                    if (index === -1) {
                        res.status(201).json({message: "Not  in the list"});


                    } else if (index !== -1) {

                        marsupilami.marsupilami_friends.splice(index, 1);

                        Marsupilami.updateOne({'_id': req.userID}, {
                            marsupilami_friends: marsupilami.marsupilami_friends
                        }, function (err, affected, resp) {
                            res.status(201).json({message: "Friend deleted"});
                        });
                    }
                } else {
                    res.status(201).json({message: "Empty friend list"});

                }

            }

        });

    });
});

//------------------------------------------------------

module.exports = router;

