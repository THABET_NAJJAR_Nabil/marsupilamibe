var mongoose= require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var MarsupilamiSchema = new Schema({
    marsupilami_ID:{type:String , require:true},
    marsupilami_Race:{type:String , require:true},
    marsupilami_Famille:{type: String},
    marsupilami_Nourriture:{type: String},
    marsupilami_Age:{type: String},
    marsupilami_email:{type : String, require:true},
    marsupilami_password:{type: String, require: true},
    marsupilami_friends:{type: Array, require: true}
});

MarsupilamiSchema.methods.encryptPassword=function(password){
    return bcrypt.hashSync(password,bcrypt.genSaltSync(5),null);
};
MarsupilamiSchema.methods.validPassword=function(password){
    return bcrypt.compareSync(password,this.marsupilami_password);
};

module.exports= mongoose.model('Marsupilami', MarsupilamiSchema);
