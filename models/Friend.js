var mongoose= require('mongoose');
var Schema = mongoose.Schema;


var FriendSchema = new Schema({
    friend_RealID:{type:String },
    friend_email:{type:String },
    friend_Race:{type:String },
    friend_Famille:{type: String},
    friend_Nourriture:{type: String},
    friend_Age:{type: String},
});

module.exports= mongoose.model('Friend', FriendSchema);
